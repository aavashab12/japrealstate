from django.urls import path
from .import views
urlpatterns=[
	path('',views.listRealtors,name='listRealtors'),
	path('<int:realtorid>',views.viewRealtor,name='viewRealtor'),
]