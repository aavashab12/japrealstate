from django.db import models
from realtor.models import Realtor

# Create your models here.
class Listing(models.Model):
	merchant=models.ForeignKey(Realtor,on_delete=models.DO_NOTHING)
	title=models.CharField(max_length=225)
	description=models.TextField(blank=True)
	address=models.CharField(max_length=225)
	price=models.IntegerField()
	negotiable=models.BooleanField(default=False)
	sqft=models.DecimalField(max_digits=3,decimal_places=2)
	floors=models.IntegerField()
	rooms=models.IntegerField()
	road=models.DecimalField(max_digits=2,decimal_places=1)
	hall=models.BooleanField(default=False)
	bathroom=models.IntegerField()
	kithen=models.IntegerField()
	garage=models.BooleanField(default=False)
	compound=models.BooleanField(default=False)
	photo_main=models.ImageField(upload_to="%y/%m/%d")
	photo_1=models.ImageField(upload_to="%y/%m/%d",blank=True)
	photo_2=models.ImageField(upload_to="%y/%m/%d",blank=True)
	photo_3=models.ImageField(upload_to="%y/%m/%d",blank=True)
	photo_4=models.ImageField(upload_to="%y/%m/%d",blank=True)
	photo_5=models.ImageField(upload_to="%y/%m/%d",blank=True)
	def __str__(self):
		return self.title